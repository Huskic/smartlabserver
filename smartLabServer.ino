#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

ESP8266WebServer server;
uint8_t pin_led = 16;
char* ssid = "mojatv_full_2878";
char* password = "SOYEFVRXSVSOYEFVRXSV";

const char webpage[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <style>
    html {
     font-family: Arial;
     display: inline-block;
     margin: 0px auto;
     text-align: center;
    }
    h2 { font-size: 3.0rem; }
    p { font-size: 3.0rem; }
    .units { font-size: 1.2rem; }
    .dht-labels{
      font-size: 1.5rem;
      vertical-align:middle;
      padding-bottom: 15px;
    }
  </style>
</head>
<body>
<h2>Smart Lab Parameters:</h2>
  <p>
    <i class="fas fa-thermometer-half" style="color:#059e8a;"></i> 
    <span class="dht-labels">Temperature</span> 
    <span id="temperature">--</span>
    <sup class="units">&deg;C</sup>
  </p>
    <p>
    <i class="fas fa-tint" style="color:#00add6;"></i> 
    <span class="dht-labels">Humidity</span>
    <span id="humidity">--</span>
    <sup class="units">%</sup>
  </p>
  <p>
    <i class="fas fa-industry"></i>
    <span class="dht-labels">CO2</span>
    <span id="carbon">--</span>
    <sup class="units">ppm</sup>
  </p>
  <p>
    <i class="fas fa-bell"></i>
    <span class="dht-labels">Alarm</span>
    <span id="alarm">OFF</span>
  </p>
</body>

<script>

  setInterval(function ( ) {
    console.log("Set temperature");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("temperature").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/temperature", true);
  xhttp.send();
  }, 4000 ) ;

  setInterval(function ( ) {
    console.log("Set humidity");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("humidity").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/humidity", true);
  xhttp.send();
  }, 4000 ) ;

  setInterval(function ( ) {
    console.log("Set carbon");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("carbon").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/carbon", true);
  xhttp.send();
  }, 4000 ) ;

  setInterval(function ( ) {
    console.log("Set alarm");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("alarm").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/alarm", true);
  xhttp.send();
  }, 4000 ) ;

</script>
</html>
)rawliteral";

float temperature = 0;
float humidity = 0;
float gas = 0;
int alarmValue = 0;

void getValuesFromArduino() {
  DynamicJsonDocument doc(1024);
  boolean messageReady = false;
  String message = "";  
  Serial.println("Entrance getValuesFromArduino");
  
  while(messageReady == false) {
    if(Serial.available()) {
      message = Serial.readString();
      messageReady = true;
    }
  }

  Serial.println(message);
    
  DeserializationError error = deserializeJson(doc,message);
  if(error) {
    Serial.print(F(" ESP8266 deserializeJson() failed: "));
    Serial.println(error.c_str());
  } else {
    temperature = doc["temperature"];
    humidity = doc["humidity"];
    gas = doc["gas"];
    alarmValue = doc["alarm"];
    Serial.println("Alarm:" + alarmValue);
  }  
}

void getTemperatureValue() {
  getValuesFromArduino();
  server.send(200, "text/plain", String(temperature));
}

void getHumidityValue() {
  server.send(200, "text/plain", String(humidity));
}

void getCarbonValue() {
  server.send(200, "text/plain", String(gas));
}

void getAlarmValue() {
  String alarm = "OFF";

  switch(alarmValue) {
    case 0:
      alarm = "OFF";
      break;  
    case 1:
      alarm = "High Temperature";
      break;
    case 2:
      alarm = "High Humidity";
      break;
    case 3:
      alarm = "High CO2 gas";
      break;
    case 4:
      alarm = "Flame alarm";
      break;
    default:
      alarm = "OFF";
      break;    
  }
  
  server.send(200, "text/plain", alarm);  
}

void setup()
{
  pinMode(pin_led, OUTPUT);
  WiFi.begin(ssid,password);
  Serial.begin(9600);
  while(WiFi.status()!=WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  
  server.on("/",[](){server.send_P(200,"text/html", webpage);});
  server.on("/temperature", getTemperatureValue);
  server.on("/humidity", getHumidityValue);
  server.on("/carbon", getCarbonValue);
  server.on("/alarm", getAlarmValue); 

  server.begin();
}

void loop()
{
  server.handleClient();
}
